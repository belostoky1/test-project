package com.open_uni.java_course.tutorials;

import java.util.Scanner;

public class UserIO {


    public static void main(String[] args) {

        DialogExample.getInputsFromJOptionPane();

        Scanner userInput = new Scanner(System.in);
        String firstName;
        System.out.print("Enter your first name: ");
        firstName = userInput.next();

        String familyName;
        System.out.print("Enter your family name: ");
        familyName = userInput.next();

        String fullName;
        fullName = firstName + " " + familyName;
        System.out.println("You are "+ fullName);
    }
}
