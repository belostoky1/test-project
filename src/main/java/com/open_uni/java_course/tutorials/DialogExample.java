package com.open_uni.java_course.tutorials;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class DialogExample {

    public static void getInputsFromScanner()
    {
        String name;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter your name > ");
        name = input.nextLine();
        System.out.print("You entered your name as: ");
        System.out.println(name);

    }

    public static void getInputsFromJOptionPane()
    {
        String name;
        name = JOptionPane.showInputDialog(null,
                "Please enter your name");
        JOptionPane.showMessageDialog(null,"Hi "+ name);

    }

    public static void main(String args[])
    {
        getInputsFromScanner();
        getInputsFromJOptionPane();
    }

}