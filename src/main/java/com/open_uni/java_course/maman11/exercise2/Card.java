package com.open_uni.java_course.maman11.exercise2;

import java.util.*;

public class Card
{
    private final String face;
    private final String suit;
    private final int numericValue;

    //This class
    public Card(String cardFace, String cardSuit)
    {
        this.face = cardFace;
        this.suit = cardSuit;
        this.numericValue = initNumericValue();
    }


    public String toString() {
        return face+" OF "+suit;
    }
    public int getNumericValue(){
        return numericValue;
    }

    private int initNumericValue() {

        switch (this.face) {
            case "Ace":
                return 14;
            case "Deuce":
                return 2;
            case "Three":
                return 3;
            case "Four":
                return 4;
            case "Five":
                return 5;
            case "Six":
                return 6;
            case "Seven":
                return 7;
            case "Eight":
                return 8;
            case "Nine":
                return 9;
            case "Ten":
                return 10;
            case "Jack":
                return 11;
            case "Queen":
                return 12;
            case "King":
                return 13;
        }
        return -1;
    }
}