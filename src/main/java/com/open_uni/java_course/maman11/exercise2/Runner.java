package com.open_uni.java_course.maman11.exercise2;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

public class Runner {

    private static StringBuilder summaryMessage;
    private static Queue<Card> player1Deck;
    private static Queue<Card> player2Deck;
    private static List<Card> currentRoundJackpot;
    private static boolean currentRoundWinner; //true=player1, false=player2
    private static boolean gameOver;
    private static int roundsCount;

    public static void main(String[] args) {

        //init variables
        roundsCount = 1;
        gameOver = true;
        summaryMessage = new StringBuilder();

        DeckOfCards deck = new DeckOfCards();
        deck.shuffle();

        player1Deck = new LinkedList<>();
        player2Deck = new LinkedList<>();
        dealCards(deck, player1Deck, player2Deck);

        currentRoundJackpot = new ArrayList<>();

        //runner method
        try {
            runWarGame();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,  e.getMessage(), "Something went wrong", JOptionPane.ERROR_MESSAGE);
        }

    }

    private static void runWarGame() {

        while (player1Deck.size() > 0 && player2Deck.size() > 0 && gameOver) {
            summaryMessage.append("\n\n");

            Card player1Card = player1Deck.poll();
            Card player2Card = player2Deck.poll();
            currentRoundJackpot.add(player1Card);
            currentRoundJackpot.add(player2Card);

            summaryMessage.append(player1Card.toString()).append(" vs ").append(player2Card.toString());
            playARound(player1Card, player2Card);
            appendMessage(roundsCount);
            roundsCount++;
            currentRoundJackpot.clear();

            if (roundsCount % 1000 == 0) {
                showSummaryMessage();
                int dialogResult = JOptionPane.showConfirmDialog (null, roundsCount + " rounds have passed. continue?");
                gameOver = (dialogResult == JOptionPane.YES_OPTION);
            }
        }

        String finalWinner = player1Deck.size() > 0 ? "player1" : "player2";
        summaryMessage.append("\n\nFinal Winner = ").append(finalWinner);
        showSummaryMessage();

    }

    private static void playARound(Card player1Card, Card player2Card) {

        int player1CardNumericValue = player1Card.getNumericValue();
        int player2CardNumericValue = player2Card.getNumericValue();
        if (player1CardNumericValue > player2CardNumericValue) {
            currentRoundWinner = true;
            collectReward();
        } else if (player1CardNumericValue < player2CardNumericValue) {
            currentRoundWinner = false;
            collectReward();
        } else {
            //its war!
            Card player1ThirdCard = prepareForWar(player1Deck);
            Card player2ThirdCard = prepareForWar(player2Deck);
            if (player1ThirdCard != null && player2ThirdCard != null) {
                playARound(player1ThirdCard, player2ThirdCard);
            } else {
                currentRoundWinner = (player1ThirdCard != null);
            }
        }
    }

    private static Card prepareForWar(Queue<Card> playerDeck) {
        Card playerFirstCard = playerDeck.poll();
        Card playerSecondCard = playerDeck.poll();
        Card playerThirdCard = playerDeck.poll();
        if (playerFirstCard == null  || playerSecondCard == null || playerThirdCard == null) {
            return null;
        }
        currentRoundJackpot.add(playerFirstCard);
        currentRoundJackpot.add(playerSecondCard);
        currentRoundJackpot.add(playerThirdCard);
        return playerThirdCard;
    }

    private static void collectReward() {
        Queue<Card> deckToModify = currentRoundWinner ? player1Deck : player2Deck;
        for (Card card : currentRoundJackpot) {
            deckToModify.offer(card);
        }
    }

    private static void dealCards(DeckOfCards deck, Queue<Card> player1Deck, Queue<Card> player2Deck) {
        for (int i = 0; i < DeckOfCards.NUMBER_OF_CARDS / 2; i++) {
            player1Deck.add(deck.dealCard());
            player2Deck.add(deck.dealCard());
        }
    }

    private static void showSummaryMessage(){
        JTextArea textArea = new JTextArea(summaryMessage.toString());
        textArea.setFont(new Font("Arial", Font.BOLD, 28));
        JScrollPane scrollPane = new JScrollPane(textArea);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        scrollPane.setPreferredSize( new Dimension( 2000, 1000 ) );
        JOptionPane.showMessageDialog(null, scrollPane, "Summary", JOptionPane.PLAIN_MESSAGE);

    }

    private static void appendMessage(int roundsCount) {
        String winner = currentRoundWinner ? "player1" : "player2";
        summaryMessage.append("\nwinner of round ").append(roundsCount).append(" is ").append(winner).append(". jackpot:\n").append(currentRoundJackpot.toString());
        summaryMessage.append("\ncurrent balance: player1 has ").append(player1Deck.size()).append(" cards, player2 has ").append(player2Deck.size());

    }
}
