package com.open_uni.java_course.maman11.exercise1;

import java.util.*;

public class BullsEye {

    private String target;
    private Map<Character, Integer> helperMap;

    public BullsEye(){

        helperMap = new HashMap<>();
        initRandomPIN();
        initHelperMap();
    }

    private void initRandomPIN() {
        List<Integer> numbers = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            numbers.add(i);
        }
        Collections.shuffle(numbers);

        StringBuilder targetSb = new StringBuilder();
        for(int i = 0; i < 4; i++){
            targetSb.append(numbers.get(i).toString());
        }
        this.target = targetSb.toString();
        System.out.println("target = " + target);
    }

    private void initHelperMap(){
        for (int i = 0; i < target.length(); i++) {
            helperMap.put(target.charAt(i), i);
        }
    }

    public String getTarget() {
        return target;
    }

    public int[] checkGuess(String guess) {

        int[] retVal= new int[2];
        int exactHits = 0;
        int nonExactHits = 0;
        for (int i = 0; i < guess.length(); i++) {
            Character currChar = guess.charAt(i);
            Integer index = helperMap.get(currChar);
            if (index != null) {
                if (index == i) {
                    exactHits++;
                }
                if (index != i) {
                    nonExactHits++;
                }
            }
        }
        retVal[0] = exactHits;
        retVal[1] = nonExactHits;
        return retVal;
    }

    public void isValidInput(String currGuessStr) throws IllegalArgumentException {

        try {
            Integer.parseInt(currGuessStr);
        } catch (Exception e) {
            throw new IllegalArgumentException("Input must be consisted of digits only");
        }

        if (currGuessStr.length() != 4) {
            throw new IllegalArgumentException("Input must be of length 4");
        }

        Set<Character> helper = new HashSet<>();
        for (int i = 0; i < currGuessStr.length(); i++) {
            helper.add(currGuessStr.charAt(i));
        }
        if (helper.size() < 4) {
            throw new IllegalArgumentException("Input must be consisted of 4 distinct digits");
        }
    }
}
