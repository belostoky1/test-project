package com.open_uni.java_course.maman11.exercise1;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;

public class Runner {

    public static void main(String[] args) {


        start();
        boolean playAgain = true;
        while (playAgain) {
            runGame();
            int dialogResult = JOptionPane.showConfirmDialog (null, "Play again?");
            playAgain = (dialogResult == JOptionPane.YES_OPTION);
        }
        System.exit(0);
    }

    private static void start(){
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Arial", Font.BOLD, 28)));
        UIManager.put("OptionPane.minimumSize",new Dimension(400,200));
        UIManager.getDefaults().put("TextArea.font", UIManager.getFont("TextField.font"));
    }

    private static void runGame() {
        int numberOfAttempts = 0;
        StringBuilder summaryMessage = new StringBuilder();
        summaryMessage.append("Summary: \n" );
        BullsEye bullsEye = new BullsEye();
        String target = bullsEye.getTarget();
        while (true) {

            String currGuess = JOptionPane.showInputDialog(new JFrame(), "Please enter your guess");
            if (currGuess == null) {
                //handle X button
                break;
            }

            if (currGuess.equals(target)) {
                numberOfAttempts++;
                String finalMessage =
                        "CORRECT!\n" +
                        "Total number of attempts: " + numberOfAttempts + "\n" +
                        summaryMessage.toString();
                JOptionPane.showMessageDialog(null, finalMessage);
                break;
            }

            try {
                bullsEye.isValidInput(currGuess);
            } catch (IllegalArgumentException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
                continue;
            }

            int[] res = bullsEye.checkGuess(currGuess);
            int exactHits = res[0];
            int nonExactHits = res[1];
            numberOfAttempts++;
            String message = exactHits + " exact hits and "+ nonExactHits + " non exact hits.";
            summaryMessage.append("Attempt number " + numberOfAttempts + " ("+currGuess+"): "+ message + "\n");
            JOptionPane.showMessageDialog(null, "You had " + message + "\n" + summaryMessage);
        }
    }
}
