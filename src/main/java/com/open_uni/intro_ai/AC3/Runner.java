package com.open_uni.intro_ai.AC3;

import java.util.*;

public class Runner {
    static String[] assignment;
    static final String[] vars = { "A1", "A2", "A3", "D1", "D2", "D3" };
    static LinkedList<String>[] domains;

    @SuppressWarnings("unchecked")
    public static void main(String args[]) {
        assignment = new String[6];
        domains = (LinkedList<String>[]) new LinkedList[6];
        String[] words = { "add", "ado", "age", "ago", "aid", "ail", "aim", "air", "and", "any", "ape", "apt", "arc",
                "are", "ark", "arm", "art", "ash", "ask", "auk", "awe", "awl", "aye", "bad", "bag", "ban", "bat", "bee",
                "boa", "ear", "eel", "eft", "far", "fat", "fit", "lee", "oaf", "rat", "tar", "tie" };
        for (int i = 0; i < 6; i++) {
            domains[i] = new LinkedList<String>(Arrays.asList(Arrays.copyOf(words, words.length)));
        }
        if (backtrack(0)) {
            for (int i = 0; i < 6; i++)
                System.out.println(vars[i] + ":" + assignment[i]);
        } else
            System.out.println("failed");
    }

    public static boolean backtrack(int indent) {
        boolean done = true;
        for (int i = 0; i < 6; i++)
            if (assignment[i] == null)
                done = false;
        if (done)
            return done;
        int var = selectVar();
        for (String val : orderDomain(var)) {
            LinkedList<String>[] backup = (LinkedList<String>[]) new LinkedList[6];
            for (int i = 0; i < 6; i++) {
                backup[i] = new LinkedList<String>(domains[i]);
            }
            if (checkConsistency(var, val)) {
                String output = "";
                for (int i = 0; i < indent; i++)
                    output += "\t";
                System.out.println(output + "trying " + vars[var] + ":" + val);
                assignment[var] = val;
                if (inferences(var,indent+1)) {
                    if (backtrack(indent + 1))
                        return true;
                }
            }
            assignment[var] = null;
            domains=backup;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private static boolean inferences(int var, int indent) {
        // return true if inferences are okay
        if (var < 3) {
            LinkedList<String>[] newdomains = (LinkedList<String>[]) new LinkedList[3];
            for (int i = 0; i < 3; i++) {
                newdomains[i] = new LinkedList<String>();
            }
            for (int i = 3; i < 6; i++) {
                if(assignment[i]!=null)
                    continue;
                for(String str : domains[i])
                {
                    if(str != assignment[var] && str.charAt(var) == assignment[var].charAt(i - 3))
                        newdomains[i-3].add(str);
                }
                if(newdomains[i-3].size()==0)
                {
                    String output = "";
                    for (int j = 0; j < indent; j++)
                        output += "\t";
                    System.out.println(output+"Error: Domain of "+vars[i]+" is empty");
                    return false;
                }
            }
            for(int i=3;i<6;i++)
                if(assignment[i]==null)
                    domains[i]=newdomains[i-3];
        } else{
            LinkedList<String>[] newdomains = (LinkedList<String>[]) new LinkedList[3];
            for (int i = 0; i < 3; i++) {
                newdomains[i] = new LinkedList<String>();
            }
            for (int i = 0; i < 3; i++) {
                if(assignment[i]!=null)
                    continue;
                for(String str : domains[i])
                {
                    if(str != assignment[var] && str.charAt(var-3) == assignment[var].charAt(i))
                        newdomains[i].add(str);
                }
                if(newdomains[i].size()==0)
                {
                    String output = "";
                    for (int j = 0; j < indent; j++)
                        output += "\t";
                    System.out.println(output+"Error: Domain of "+vars[i]+" is empty");
                    return false;
                }
            }
            for(int i=0;i<3;i++)
                if(assignment[i]==null)
                    domains[i]=newdomains[i];
        }

        return true;
    }

    private static LinkedList<String> orderDomain(int var) {
        return domains[var];// this is fake
    }

    private static boolean checkConsistency(int var, String val) {
        // maybe enforce arc-consistency!
        if (var < 3) {
            for (int i = 3; i < 6; i++) {
                if (assignment[i] != null) {
                    if (assignment[i] == val || assignment[i].charAt(var) != val.charAt(i - 3))
                        return false;
                }
            }
        } else
            for (int i = 0; i < 3; i++)
                if (assignment[i] != null)
                    if (assignment[i] == val || assignment[i].charAt(var - 3) != val.charAt(i))
                        return false;
        return true;
    }

    private static int selectVar() {
        int mini = -1;
        boolean unique = true;
        for (int i = 0; i < 6; i++) {
            if (assignment[i] != null)
                continue;
            if (mini == -1 || domains[i].size() < domains[mini].size()) {
                mini = i;
                unique = true;
            } else if (mini != -1 && domains[i].size() == domains[mini].size())
                unique = false;
        }
        if (mini != -1 && unique)
            return mini;
        mini = -1;
        int mincount = 0, count;
        for (int i = 0; i < 6; i++) {
            if (assignment[i] != null)
                continue;
            count = 0;
            if (i < 3) {
                for (int j = 3; j < 6; j++) {
                    if (assignment[j] == null)
                        count++;
                }
            } else {
                for (int j = 0; j < 3; j++) {
                    if (assignment[j] == null)
                        count++;
                }
            }
            if (count >= mincount) {
                mini = i;
                mincount = count;
            }
        }

        return mini;
    }
}
