package com.exercises.strings;

/**
 * Created by User on 4/12/2016.
 */
public class Reverse {

    public static void main (String [] args) {

        String inputStr = "abcd";
        char[] input = inputStr.toCharArray();

        iterative(input);
        String str = new String(input);
        System.out.println("str = " + str);

        String recursiveSol = recursive(inputStr);
        System.out.println("recursiveSol = " + recursiveSol);

    }

    private static String recursive(String input) {

        if (input.length()==1) {
            return input;
        } else {
            return input.charAt(input.length()-1) + recursive(input.substring(0,input.length()-1));
        }
    }

    private static void iterative(char[] input) {

        for (int i = 0; i < input.length /2; i++) {

            swap(input,i,input.length-i-1);
        }

    }

    private static void swap(char[] input,int i, int j) {

        char c1 = input[i];
        char c2 = input[j];
        input[i] = c2;
        input[j] = c1;

    }


}
