package com.exercises.strings;

import java.util.Arrays;

/**
 * Created by User on 4/11/2016.
 */
public class FreqChar {

    public static void main (String [] args) {

        String inputStr = "hlllhelloooohh";
        char[] input = inputStr.toCharArray();

        int result = count(input);
        System.out.println("result = " + result);

    }

    //O(nlogn) time. O(1) space
    private static int count(char[] input) {

        int currCount = 1;
        int maxCount = 1;
        Arrays.sort(input);

        String str = new String(input);
        System.out.println("str = " + str);

        for (int i = 0; i < input.length - 1; i++) {
            char curr = input[i];
            char next = input[i + 1];

            if (curr==next) {
                currCount++;
            } else {
                currCount = 1;
            }

            if (currCount > maxCount) {
                maxCount = currCount;
            }
        }

        return  maxCount;
    }

}
