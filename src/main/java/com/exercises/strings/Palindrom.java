package com.exercises.strings;

/**
 * Created by User on 4/13/2016.
 */
public class Palindrom {

    public static void main (String [] args) {

        String palindromTrue =  "abdgdba";
        String palindromFalse =  "abdgdbaaaaa";

        boolean result = recursive(palindromTrue);
        System.out.println("result = " + result);

        result = recursive(palindromFalse);
        System.out.println("result = " + result);


    }

    private static boolean recursive(String palindrom) {

        if (palindrom.length() == 0 || palindrom.length() == 1) {
            return true;
        }

        char first = palindrom.charAt(0);
        char last = palindrom.charAt(palindrom.length()-1);
        String residue = palindrom.substring(1,palindrom.length()-1);


        boolean flag = (first == last);

        return flag && recursive(residue);

    }




}
