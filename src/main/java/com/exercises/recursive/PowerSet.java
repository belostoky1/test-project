package com.exercises.recursive;

import java.util.HashSet;
import java.util.Set;
import java.util.zip.Inflater;

/**
 * Created by User on 4/11/2016.
 */
public class PowerSet {

    public static void main(String [] args) {

        Set<Integer> set = new HashSet<Integer>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
//        Set<Set<Integer>> ps = toPowerSet(set);
        Set<Set<Integer>> ps = toPowerSetRecursive(set);
        System.out.println("ps = " + ps);

    }

    private static Set<Set<Integer>> toPowerSet(Set<Integer> set) {
        Set<Set<Integer>> retVal = new HashSet<Set<Integer>>();

        retVal.add(new HashSet<Integer>());

        for (Integer item : set) {

            Set<Set<Integer>> newPS = new HashSet<Set<Integer>>();

            for (Set<Integer> subset : retVal) {

                newPS.add(subset);

                Set<Integer> newSubset = new HashSet<Integer>(subset);
                newSubset.add(item);
                newPS.add(newSubset);

                newPS.add(newSubset);
            }

            retVal = newPS;

        }
        return retVal;
    }

    private static Set<Set<Integer>> toPowerSetRecursive(Set<Integer> set) {
        Set<Set<Integer>> retVal = new HashSet<Set<Integer>>();

        Set<Integer> inputCopy = new HashSet<Integer>(set);

        if (set == null) {
            return null;
        }

        if (set.size() == 0) {
            retVal.add(set);
            return retVal;
        }

        if (set.size() == 1) {
            retVal.add(set);
            return retVal;
        }

        retVal.add(new HashSet<Integer>());

        for (Integer item : set) {

            Set<Integer> tmp = new HashSet<Integer>();
            tmp.add(item);
            retVal.add(tmp);

            inputCopy.remove(item);
            retVal.addAll(toPowerSetRecursive(inputCopy));
            inputCopy.add(item);

        }
        return  retVal;

    }

}
