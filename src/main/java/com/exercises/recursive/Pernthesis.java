package com.exercises.recursive;

/**
 * Created by User on 4/11/2016.
 */
public class Pernthesis {


    public static void main (String [] args) {


        int input = 3;

        printAllLegalParenthesis(input);


    }

    private static void printAllLegalParenthesis(int input) {

        brackets(input,0, "");

    }

    static void brackets(int left, int right, String s) {
        if (left == 0 && right == 0) {
            System.out.print(s+",");
        }
        if (left > 0) {
            brackets(left-1, right+1, s + "<");
        }
        if (right > 0) {
            brackets(left, right-1, s + ">");
        }
    }
}
