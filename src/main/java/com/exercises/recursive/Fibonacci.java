package com.exercises.recursive;

/**
 * Created by User on 4/11/2016.
 */
public class Fibonacci {

    public static void main(String [] args) {

        int f1 = iterative(3);
        System.out.println("f1 = " + f1);

//        int f2 = recursive(7);
//        System.out.println("f2 = " + f2);

    }

    private static int recursive(int n) {

        int retVal;

        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }

        retVal = recursive(n-1) + recursive(n-2);

        return  retVal;

    }

    private static int iterative(int n) {

        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }

        int prev = 0;
        int curr = 1;
        int next = 0;
        int count = 0;
        while (count < n) {

            next = prev + curr;
            prev = curr;
            curr = next;
            count++;
        }

        return prev;

    }

}
