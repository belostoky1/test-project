package com.exercises.recursive;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by User on 4/11/2016.
 */
public class DimesNickles {

    public static void main (String [] args) {

        int callback = makeChange1(15,25);
        System.out.println("callback = " + callback);

    }

    public static int makeChange1(int n, int denom) {
        int next_denom = 0;
        switch (denom) {
            case 25:
                next_denom = 10;
                break;
            case 10:
                next_denom = 5;
                break;
            case 5:
                next_denom = 1;
                break;
            case 1:
                return 1;
        }
        int ways = 0;
        for (int i = 0; i * denom <= n; i++) {
            ways += makeChange1(n - i * denom, next_denom);
        }
        return ways;
    }



}
