package com.exercises.recursive;

import java.util.Stack;

/**
 * Created by User on 4/11/2016.
 */
public class ArithmeticEval {

    public static void main (String [] args){


        String input = "( 1 + ( ( 2 + 3 ) * ( 4 * 5 ) ) ) ";
        Double result = eval(input);
        System.out.println("result = " + result);

    }

    private static Double eval(String input) {

        Stack<String> ops  = new Stack<String>();
        Stack<Double> vals = new Stack<Double>();

        for (int i = 0; i < input.length(); i++) {

            String s = String.valueOf(input.charAt(i));
            System.out.println("s = " + s);


            if      (s.equals("("))               ;
            else if (s.equals("+"))    ops.push(s);
            else if (s.equals("-"))    ops.push(s);
            else if (s.equals("*"))    ops.push(s);
            else if (s.equals("/"))    ops.push(s);
            else if (s.equals("sqrt")) ops.push(s);
            else if (s.equals(")")) {
                String op = ops.pop();
                double v = vals.pop();
                if      (op.equals("+"))    v = vals.pop() + v;
                else if (op.equals("-"))    v = vals.pop() - v;
                else if (op.equals("*"))    v = vals.pop() * v;
                else if (op.equals("/"))    v = vals.pop() / v;
                else if (op.equals("sqrt")) v = Math.sqrt(v);
                vals.push(v);
            }
            else vals.push(Double.parseDouble(s));
        }

        return vals.pop();
    }


}
