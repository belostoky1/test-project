package com.exercises.recursive;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by User on 4/11/2016.
 */
public class Permutations {

    public static void main (String [] args) {

        String input = "guy";
        Set<String> allPerumtations = recursive(input);

        System.out.println("allPerumtations = " + allPerumtations);


    }

    private static Set<String> recursive(String input) {

        Set<String> retVal = new HashSet<String>();

        if (input.length() == 0) {
            retVal.add(input);
        } else if (input.length() == 1) {
            retVal.add(input);
        } else {

            String substring = input.substring(1, input.length());
            Set<String> recursive = recursive(substring);


            for (int i = 0; i < input.length(); i++) {

                char c = input.charAt(i);
                String inputTmp = input.substring(0,i) + input.substring(i+1,input.length());
                Set<String> callback = recursive(inputTmp);
                for (String s : callback) {
                    retVal.add(c+s);
                }

            }

        }

        return retVal;

    }
}
