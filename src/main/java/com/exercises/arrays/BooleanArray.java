package com.exercises.arrays;

/**
 * Created by User on 4/12/2016.
 */
public class BooleanArray {


    public static void main (String [] args) {



        boolean[] a = new boolean[10];
        a[3]  =true;
        a[5]  =true;
        a[8]  =true;
        printArr(a);

        System.out.println();
        foo(a);

        printArr(a);


    }

    private static void foo(boolean[] a) {

        int i = 2;

        for (i= i ; i<a.length; i++){
            for(int j=i; j<a.length; j=j+i){
                a[i] = !a[j];
            }
        }
    }

    public static void printArr(boolean[] arr) {

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }

}
