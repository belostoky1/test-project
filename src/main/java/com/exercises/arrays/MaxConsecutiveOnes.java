package com.exercises.arrays;

/**
 * Created by User on 10/21/2017.
 */
public class MaxConsecutiveOnes {

    public static void main (String[] args){

        int[] nums = new int[]{1,1,1,0,0,1,0,1,0,1,1,0,1,1,1,1,1};
        int res = foo(nums);
        System.out.println("res = " + res);
    }
    private static int foo(int[] input) {

        int currMax = 0;
        int retVal = 0;
        for (int i = 0; i < input.length; i++) {
            if(input[i]==1) {
                currMax++;
            } else {
                if (currMax>retVal){
                    retVal=currMax;
                }
                currMax=0;
            }
        }

        if (currMax > retVal) {
            return currMax;
        }
        return retVal;
    }
}
