package com.exercises.arrays;

/**
 * Created by User on 10/21/2017.
 */
public class ProductOfArrayExceptSelf {

    public static void main(String[] args) {

        int [] input = new int[] {1,2,3,4};
        foo(input);
    }

    private static void foo(int[] input) {

        int curr = 1;
        int[] prev = new int[input.length];
        int[] retVal = new int[input.length];
        for (int i = 0; i < input.length; i++) {

            curr*=input[i];
            retVal[i]=curr;

        }
        printArr(retVal);
    }

    public static void printArr(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}
