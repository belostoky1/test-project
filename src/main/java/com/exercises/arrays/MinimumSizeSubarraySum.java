package com.exercises.arrays;

import java.util.Arrays;

/**
 * Created by User on 10/21/2017.
 */
public class MinimumSizeSubarraySum {

    public static void main(String[] args) {

        int [] input = new int[] {2,3,1,2,4,3};
        int x =foo(input, 3);
        System.out.println("x = " + x);
    }

    //O(n^2)
    private static int foo(int[] input, int s) {

        int shortest = 100000;
        for (int i = 0; i < input.length; i++) {
            int curr = 0;
            for (int j = i; j < input.length; j++) {
                curr+=input[j];
                if (curr>=s) {
                    if (shortest > (j - i +1)) {
                        shortest=(j-i) + 1;
                    }
                    break;
                }
            }
        }
        if (shortest == 100000) {
            return 0;
        }
        return shortest;
    }


}
