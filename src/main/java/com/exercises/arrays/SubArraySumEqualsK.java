package com.exercises.arrays;

/**
 * Created by User on 10/21/2017.
 */
public class SubArraySumEqualsK {

    public static void main(String[] args){

        //int[] arr = new int[]{1,1,2,1,3};
        int[] arr = new int[]{1,1,1};
        int k = 2;
        int res = foo(arr, k);
        System.out.println("res = " + res);

    }

    private static int foo(int[] arr, int k) {

        int retVal = 0;
        int currSum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j <= k; j++) {
                currSum += arr[j];
                if (currSum == k) {
                    retVal++;
                    break;
                }
                if (currSum > k) {
                    break;
                }
            }
            currSum = 0;
        }
        return retVal;
    }

}
