package com.exercises.arrays;

/**
 * Created by User on 10/31/2017.
 */
public class MaxSumSubarray {

    public static void main(String[] args) {

        int [] input = new int[] {0,-1, 1, 2 ,-13 ,2};
        foo(input);
    }


    private static void foo(int[] input) {

        int maxSoFar = input[0];
        int maxEndingHere = input[0];
        for (int i = 1; i < input.length; i++) {

            maxEndingHere = Math.max(maxEndingHere+ input[i], input[i]);
            maxSoFar = Math.max(maxSoFar, maxEndingHere);
        }

        System.out.println("max = " + maxEndingHere);
        printArr(input);
    }


    public static void printArr(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}
