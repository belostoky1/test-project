package com.exercises.arrays;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class ArrayDegree {

    public static void main(String[] args){

        int[] arr = new int[]{1,2,2,3,1,4,2};
        int res = secondLowestValue(arr);
        System.out.println("res = " + res);

    }

    public static int secondLowestValue(int[] input) {

        int min = input[0];
        int retVal = input[0];
        for (int i = 1; i<input.length; i++) {
            if (min > input[i]) {
                retVal = min;
                min = input[i];
            }
        }
        return retVal;
    }

    public static int findShortestSubArray(int[] nums) {
        Map<Integer, Integer> left = new HashMap(),
                right = new HashMap(), count = new HashMap();

        for (int i = 0; i < nums.length; i++) {
            int x = nums[i];
            if (left.get(x) == null) left.put(x, i);
            right.put(x, i);
            count.put(x, count.getOrDefault(x, 0) + 1);
        }

        int ans = nums.length;
        int degree = Collections.max(count.values());
        for (int x: count.keySet()) {
            if (count.get(x) == degree) {
                ans = Math.min(ans, right.get(x) - left.get(x) + 1);
            }
        }
        return ans;
    }

    private static int findShortestSubArrayBruteForce(int[] nums) {

        int degree = getArrayDegree(nums);
        System.out.println("degree = " + degree);

        int retVal = nums.length;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+degree; j < nums.length; j++) {
                int[] subArray = Arrays.copyOfRange(nums, i, j + 1);
                int subArrDegree = getArrayDegree(subArray);
                if (subArrDegree == degree && subArray.length<retVal) {
                    retVal=subArray.length;
                }
            }
        }
        return retVal;
    }

    private static int getArrayDegree(int[] nums){

        Map<Integer, Integer> map = new HashMap<Integer,Integer>();
        for (int i = 0; i<nums.length;i++) {
            if (map.containsKey(nums[i])) {
                int freq = map.get(nums[i]);
                freq++;
                map.put(nums[i],freq);
            } else {
                map.put(nums[i],1);
            }
        }

        int retVal = 1;
        for (int key : map.keySet()){
            int currVal = map.get(key);
            if (currVal > retVal){
                retVal=currVal;
            }
        }
        return retVal;
    }
}
