package com.exercises.arrays;

/**
 * Created by User on 10/31/2017.
 */
public class StrStr {

    public static void main(String[] args){

        String str1 = "TestTest";
        String str2 = "stT";
        boolean res = foo(str1, str2);
        System.out.println("res = " + res);
    }

    private static boolean foo(String longString, String stringToFind) {

        if (stringToFind.length() >  longString.length() || longString.length() == 0) {
            return false;
        }
        if (stringToFind.length() == 0) {
            return true;
        }

        for (int i = 0; i < longString.length(); i++) {
            if (longString.substring(i).startsWith(stringToFind)) {
                return true;
            }
        }
        return false;
    }







}
