package com.exercises.arrays;

/**
 * Created by User on 10/21/2017.
 */
public class Triangle {

    public static void main (String[] args){

        int[][] nums = new int[][]{{2},{3,4},{6,5,7},{4,1,8,3}};
        int res = foo(nums);
        System.out.println("res = " + res);
    }
    private static int foo(int[][] input) {

        int retVal = 0;
        for (int[] arr : input){
            int currMin = min(arr);
            retVal+=currMin;
        }
        return retVal;
    }

    private static int min(int[] input){

        int retVal = input[0];
        for (int i = 1; i < input.length; i++) {
            if (input[i]<retVal){
                retVal=input[i];
            }
        }
        return retVal;
    }
}
