package com.exercises.arrays;

import java.util.*;

/**
 * Created by User on 10/21/2017.
 */
public class RandomizedSet {

    int size = 0;
    List<Integer> list;
    Map<Integer,Integer> map;
    /** Initialize your data structure here. */
    public RandomizedSet() {
        list = new ArrayList<>();
        map = new HashMap<>();
    }

    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        if (map.containsKey(val)) {
            return false;
        }
        map.put(val, size);
        list.add(val);
        size++;
        return true;
    }

    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        if (!map.containsKey(val)) {
            return false;
        }
        int indexOfTheValueToDelete = map.get(val);
        int lastValInList = list.get(list.size()-1);
        list.add(indexOfTheValueToDelete, lastValInList);
        map.remove(val);
        size--;
        return true;
    }

    /** Get a random element from the set. */
    public int getRandom() {
        Random rand = new Random();
        int randomIndex = rand.nextInt(size);
        return list.get(randomIndex);
    }
}
