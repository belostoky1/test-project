package com.exercises.arrays;

import javafx.collections.transformation.SortedList;

import java.util.*;

/**
 * Created by User on 10/22/2017.
 */
public class LongestConsecutiveSequence {

    public static void main(String[] args){

        int[] arr = new int[]{100,101,99,102,1,3,2};
        int res = foo(arr);
        System.out.println("res = " + res);

    }

    private static int foo(int[] arr) {

        Map<Integer,Integer> startValueToLengthSequences = new HashMap<>();
        Set<Integer> visited = new HashSet<>();

        for (int i = 0; i < arr.length; i++) {
            int curr = arr[i];
            if (visited.contains(curr+1)) {
                //update start-value
                int currLength = startValueToLengthSequences.get(curr);
                currLength++;
                startValueToLengthSequences.put(curr, currLength);
            }

            if (visited.contains(curr-1)) {
                //update start-value
                int currLength = startValueToLengthSequences.get(curr);
                currLength++;
                startValueToLengthSequences.put(curr-1, currLength);
                continue;
            }
            if (!visited.contains(arr[i]+1) && !visited.contains(arr[i]-1)) {
                startValueToLengthSequences.put(arr[i], 1);
            } else {

            }
        }
        return 1;
    }

}
