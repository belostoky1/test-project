package com.exercises.arrays;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 10/22/2017.
 */
public class MergeIntervals {

    public static void main(String[] args){

        List<Pair<Integer, Integer>> intervals = new ArrayList<>();
        intervals.add(new Pair<>(1,3));
        intervals.add(new Pair<>(2,6));
        intervals.add(new Pair<>(8,10));
        intervals.add(new Pair<>(15,18));
        List<Pair<Integer, Integer>> res = foo(intervals);
        System.out.println("res = " + res);

    }

    private static List<Pair<Integer, Integer>> foo(List<Pair<Integer, Integer>> intervals) {

        boolean firstIteration = true;
        List<Pair<Integer, Integer>> retVal = new ArrayList<>();
        Pair prevInterval = intervals.get(0);
        Pair<Integer, Integer> tmp = new Pair(prevInterval.getKey(), prevInterval.getValue());
        for (Pair<Integer, Integer> currInterval : intervals) {
            if (firstIteration) {
                firstIteration = false;
                continue;
            }

            int currLeft = currInterval.getKey();
            int currRight = currInterval.getValue();
            int tmpLeft = tmp.getKey();
            int tmpRight = tmp.getValue();
            if (currLeft < tmpRight) {
                tmp = new Pair(tmpLeft, currRight);
            } else {
                retVal.add(tmp);
                tmp = new Pair(currLeft, currRight);
            }
        }
        retVal.add(tmp);
        return retVal;
    }
}

