package com.exercises.arrays;

/**
 * Created by User on 4/12/2016.
 */
public class MoveZeros {


    public static void main(String[] args) {

        int [] input = new int[] {0,0, 1, 0 ,13 ,2};
        foo(input);
    }




    private static void foo(int[] input) {

        int lastNonZeroIndesx = 0;
        for (int i = 0; i < input.length; i++) {

            if (input[i] != 0) {
                swap(input, lastNonZeroIndesx, i);
                lastNonZeroIndesx++;
            }

        }

        printArr(input);
    }

    private static void moveElementFromStartToEnd(int[] input) {

        for (int i = 0; i < input.length-1; i++) {
            swap(input,i, i + 1);
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int x = arr[j];
        arr[j]=arr[i];
        arr[i]=x;
    }

    public static void printArr(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}
