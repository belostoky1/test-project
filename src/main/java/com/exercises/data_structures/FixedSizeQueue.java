package com.exercises.data_structures;


import java.util.NoSuchElementException;

/**
 * Created by User on 11/4/2017.
 */
public class FixedSizeQueue<V> {
    int capacity;
    Node head=null;
    Node tail=null;
    int size;

    class Node{
        V value;
        Node prev;
        Node next;

        public Node(V value){
            this.value = value;
        }
    }

    public FixedSizeQueue(int capacity) {
        this.capacity = capacity;
    }

    public void enqueue(V value){
        Node created = new Node(value);
        if(size >= capacity){
            remove(tail);
            setHead(created);
        }else{
            setHead(created);
        }
        if (size < capacity){
            size++;
        }
    }

    public V dequeue() throws NoSuchElementException{
        if (isEmpty()) {
            throw new NoSuchElementException("queue is empty");
        }
        V retVal = tail.value;
        remove(tail);
        size--;
        return retVal;
    }

    public void remove(Node n){
        if(n.prev !=null){
            n.prev.next = n.next;
        }else{
            //n doesn't have prev i.e n is head -> update head
            head = n.next;
        }

        if(n.next!=null){
            n.next.prev = n.prev;
        }else{
            //n doesn't have next i.e n is tail -> update tail
            tail = n.prev;
        }
    }

    public void setHead(Node n){
        n.next = head;
        n.prev = null;

        if(head!=null) {
            head.prev = n;
        }

        head = n;

        if(tail ==null) {
            //tail is not initiated
            tail = head;
        }
    }

    public boolean isEmpty(){
        return size==0;
    }

}