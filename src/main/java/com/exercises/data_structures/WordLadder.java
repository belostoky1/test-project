package com.exercises.data_structures;

import java.util.*;

/**
 * Created by User on 11/12/2017.
 */
public class WordLadder {

    public static void main(String[] args){
        String beginWord="hit";
        String endWord="cog";
        List<String> wordList = new ArrayList();

        wordList.add(beginWord);
        wordList.add("hot");
        wordList.add("dot");
        wordList.add("dog");
        wordList.add("lot");
        wordList.add("cot");
        wordList.add("log");
        wordList.add(endWord);

        Map<String,List<String>> edges = loadEdges(wordList);

        bfsPaths(edges, beginWord, endWord);
        //dfs(edges, beginWord, endWord);
    }

    private static Map<String, List<String>> loadEdges(List<String> wordList) {

        Map<String, List<String>> retVal = new HashMap<>();
        for (int i = 0; i < wordList.size(); i++) {
            String curr = wordList.get(i);
            for (int j = 0; j < wordList.size(); j++) {
                if (i==j) {
                    continue;
                }
                String candNeighbour = wordList.get(j);
                if (areNeighbours(curr, candNeighbour)) {
                    if (retVal.containsKey(curr)) {
                        retVal.get(curr).add(candNeighbour);
                    } else {
                        List<String> neighbours = new ArrayList<>();
                        neighbours.add(candNeighbour);
                        retVal.put(curr, neighbours);
                    }
                }
            }
        }
        return retVal;
    }

    private static boolean areNeighbours(String curr, String candidate) {
        int count = 0;
        for (int i = 0; i < curr.length(); i++) {
            if (curr.charAt(i) == candidate.charAt(i)){
                count++;
            }
        }
        return (count == (curr.length() - 1));
    }

    private static void bfsPaths(Map<String, List<String>> edges,String beginWord, String endWord) {

        TreeMap<Integer,List<List<String>>> shortestPaths = new TreeMap<>();

        Set<String> visited = new HashSet<>();
        List<List<String>> pathsQueue = new ArrayList<>();

        List<String> currPath = new ArrayList<>();

        currPath.add(beginWord);
        pathsQueue.add(currPath);
        visited.add(beginWord);
        while (!pathsQueue.isEmpty()){
            currPath = pathsQueue.remove(pathsQueue.size()-1);
            String last = currPath.get(currPath.size()-1);
            if (last.equals(endWord)) {
                int pathLength = currPath.size();
                List<List<String>> sameLengthPaths = shortestPaths.get(pathLength);
                if (sameLengthPaths == null) {
                    sameLengthPaths = new ArrayList<>();
                }
                sameLengthPaths.add(currPath);
                shortestPaths.put(pathLength, sameLengthPaths);
            }
            List<String> neighbours = edges.get(last);
            for (String neighbour : neighbours) {
                if (!visited.contains(neighbour) || neighbour.equals(endWord)) {
                    List<String> newPath = new ArrayList<>(currPath);
                    newPath.add(neighbour);
                    pathsQueue.add(0, newPath);
                    visited.add(neighbour);
                }
            }
        }

        System.out.println("shortestPaths = " + shortestPaths.firstEntry());
    }

    private static void bfs(Map<String, List<String>> edges,String beginWord, String endWord) {

        List<String> queue = new ArrayList<>();
        Set<String> visited = new HashSet<>();
        queue.add(beginWord);
        visited.add(beginWord);
        while (!queue.isEmpty()) {
            String curr = queue.remove(0);
            List<String> neighbours = edges.get(curr);
            for (String neighbour : neighbours) {
                if (!visited.contains(neighbour)) {
                    visited.add(neighbour);
                    queue.add(neighbour);
                }
            }
        }
    }

    private static void dfs(Map<String, List<String>> edges,String beginWord, String endWord) {

        Stack<String> stack = new Stack<>();
        Set<String> visited = new HashSet<>();
        stack.push(beginWord);
        visited.add(beginWord);
        while (!stack.isEmpty()) {
            String curr = stack.pop();
            List<String> neighbours = edges.get(curr);
            for (String neighbour : neighbours) {
                if (neighbour.equals(endWord)) {
                    System.out.println(visited.toString());
                }
                if (!visited.contains(neighbour)) {
                    visited.add(neighbour);
                    stack.push(neighbour);
                }
            }
        }
    }

    private static void dfsRecursive(Map<String, List<String>> edges, String beginWord) {

        Set<String> visited = new HashSet<>();
        visited.add(beginWord);
        for (String neighbour : edges.get(beginWord)) {
            if (!visited.contains(neighbour)) {
                dfsRecursive(edges, neighbour);
            }
        }
    }
}
