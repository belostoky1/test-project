package com.exercises.data_structures;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 11/4/2017.
 */
public class MinStack<Integer> {

    List<Integer> stack;
    int capacity;
    int size;
    int min;
    public MinStack(int capacity){
        this.stack = new LinkedList<>();
        this.capacity = capacity;
    }

    Integer peek(){
        return stack.get(0);
    }

    void push(Integer element){
        if (size >= capacity) {
            System.out.println("stack is full");
            return;
        }
        stack.add(0, element);
        size++;
    }

    void pop(){
        if (size == 0) {
            System.out.println("stack is full");
            return;
        }
        stack.remove(0);
        size--;
    }

    boolean isEmpty(){
        return size==0;
    }
}
