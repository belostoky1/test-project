package com.exercises.data_structures;

import java.util.HashMap;

/**
 * Created by User on 11/4/2017.
 */
public class LRUCache {
    int capacity;
    HashMap<Integer, Node> map = new HashMap<>();
    Node head=null;
    Node tail=null;

    class Node{
        int key;
        int value;
        Node prev;
        Node next;

        public Node(int key, int value){
            this.key = key;
            this.value = value;
        }
    }

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public void set(int key, int value) {
        if(map.containsKey(key)){
            //if key exists -> update its value and move it to the head of the queue
            Node old = map.get(key);
            old.value = value;
            remove(old);
            setHead(old);
        }else{
            //if key doesn't exist -> add its node at the head of the queue
            Node created = new Node(key, value);
            if(map.size()>=capacity){
                map.remove(tail.key);
                remove(tail);
            }
            setHead(created);
            map.put(key, created);
        }
    }

    public int get(int key) {
        if(map.containsKey(key)){
            Node n = map.get(key);
            remove(n);
            setHead(n);
            return n.value;
        }

        return -1;
    }

    public void remove(Node n){
        if(n.prev !=null){
            n.prev.next = n.next;
        }else{
            //n doesn't have prev i.e n is head -> update head
            head = n.next;
        }

        if(n.next!=null){
            n.next.prev = n.prev;
        }else{
            //n doesn't have next i.e n is tail -> update tail
            tail = n.prev;
        }
    }

    public void setHead(Node n){
        n.next = head;
        n.prev = null;

        if(head!=null) {
            head.prev = n;
        }

        head = n;

        if(tail ==null) {
            //tail is not initiated
            tail = head;
        }
    }
}