package com.exercises.data_structures;

import java.util.NoSuchElementException;

/**
 * Created by User on 11/4/2017.
 */
public class Tester {


    public static void main (String[] args){
        
        //testQueue();

        testLRUCache();
    }

    private static void testLRUCache() {

        LRUCache cache = new LRUCache(3);
        cache.set(1,10);
        cache.set(2,20);
        cache.set(3,30);
        cache.set(4,40);
        cache.set(2,40);
        int x = cache.get(1);
        System.out.println("x = " + x);
        x = cache.get(2);
        System.out.println("x = " + x);

        cache.set(3,33);
        cache.set(4,44);
        cache.set(5,55);
        x = cache.get(2);
        System.out.println("x = " + x);


    }

    private static void testQueue() {

        FixedSizeQueue<Integer> queue = new FixedSizeQueue(5);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(12);
        queue.enqueue(13);
        queue.enqueue(14);
        queue.enqueue(15);

        try {
            System.out.println(queue.dequeue());
        } catch (NoSuchElementException e){
            System.out.println(e.toString());
        }

        try {
            System.out.println(queue.dequeue());
        } catch (NoSuchElementException e){
            System.out.println(e.toString());
        }

        try {
            System.out.println(queue.dequeue());
        } catch (NoSuchElementException e){
            System.out.println(e.toString());
        }

        try {
            System.out.println(queue.dequeue());
        } catch (NoSuchElementException e){
            System.out.println(e.toString());
        }
        try {
            System.out.println(queue.dequeue());
        } catch (NoSuchElementException e){
            System.out.println(e.toString());
        }
        try {
            System.out.println(queue.dequeue());
        } catch (NoSuchElementException e){
            System.out.println(e.toString());
        }

    }
}
