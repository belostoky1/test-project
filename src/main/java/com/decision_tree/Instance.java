package com.decision_tree;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds data for particular instance. Integer values refer to offsets in meta-data arrays of a
 * surrounding DataSet.
 */
public class Instance {
  public String label;
  public List<Integer> attributes = null;

  /**
   * Add attribute values in the order of attributes as specified by the dataset
   */
  public void addAttribute(Integer i) {
    if (attributes == null) {
      attributes = new ArrayList<>();
    }
    attributes.add(i);
  }
}
