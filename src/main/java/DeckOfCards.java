import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeckOfCards {

    private List<Card> deck;
    private int currentCard;
    public static final int NUMBER_OF_CARDS = 52;

    public DeckOfCards() {
        String[] faces = { "Ace", "Deuce", "Three", "Four", "Five", "Six",
                "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
        String[] suits = { "Hearts", "Diamonds", "Clubs", "Spades" };
        deck = new ArrayList<>(); // create array of Card objects
        for (String face : faces) {
            for (String suit : suits) {
                deck.add(new Card(face, suit));
            }
        }
    }

    // shuffle deck of Cards with one-pass algorithm
    public void shuffle() {

        Collections.shuffle(deck);
    }

    public Card dealCard() {


        // determine whether Cards remain to be dealt
        if (currentCard < deck.size())
            return deck.get(currentCard++);
        else
            return null; // return null to indicate that all Cards were dealt
    }

    public String toString(){
        StringBuilder str = new StringBuilder();
        for (Card aDeck : deck) {
            str.append(aDeck).append("\n");
        }
        return str.toString();
    }
} // end class DeckOfCards